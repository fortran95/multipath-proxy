var TUNNELS = 3;
var LOCAL_TCP_PORT = 7001,
    REMOTE_TCP_PORT = 9150;

/****************************************************************************
 * Following ports are only for this system itself.
 */

var ENTRANCE_UDP_IN = 7011, // UDPTunnel connects to this at local machine
    ENTRANCE_TCP_FORWARD_BASE = 7100; // Local opens as TCP tunnel exits

var EXIT_TCP_LISTENING_BASE = 17100, // Remote opens as TCP tunnel entrances
    EXIT_UDP_OUT = 17011; // Remote connects to UDPTunnel on this port

//////////////////////////////////////////////////////////////////////////////

var childProcess = require('child_process');
var UDPDistributor = require('./libUDPDistributor.js'),
    PacketForwarder = require('./libPacketForwarder.js'),
    PacketCollector = require('./libPacketCollector.js'),
    UDPCollector = require('./libUDPCollector.js');

function buildEntrance(){
    // Build an entrance of this tunnel on local computer.
    var PacketForwarders = [], forwardToPort;
    var server;
    for(var i=0; i<TUNNELS; i++){
        forwardToPort = ENTRANCE_TCP_FORWARD_BASE + i;
        PacketForwarders.push(new PacketForwarder(forwardToPort));
    }
    server = UDPDistributor(ENTRANCE_UDP_IN, PacketForwarders);

    var tunnel = childProcess.spawn('./udptunnel', [
        '-c', // run as client
        '-vvv',  // verbose
        LOCAL_TCP_PORT,
        '127.0.0.1',
        ENTRANCE_UDP_IN,
        '127.0.0.1',
        REMOTE_TCP_PORT,
    ]);

    tunnel.stderr.pipe(process.stderr);
    tunnel.stdout.pipe(process.stdout);

    function onClose(){
        tunnel.kill('SIGINT');
        tunnel.on('close', function(){
            console.log('Local tunnel entrance closed.');
        });
        server.close();
        for(var i=0; i<PacketForwarders.length; i++){
            PacketForwarders[i].close();
        }
    }
    process.once('exit', onClose);
    process.once('SIGINT', onClose);

    var str = "UDP Packets now sending to port [" + ENTRANCE_UDP_IN + 
        "] will be delivered to one of following ports:";
    for(var i=0; i<TUNNELS; i++) str += (ENTRANCE_TCP_FORWARD_BASE + i) + " ";
    console.log(str);
}

function buildExit(){
    // Build an exit of this tunnel on a remote server.
    var PacketCollectors = [], listenOnPort;
    var client;
    for(var i=0; i<TUNNELS; i++){
        listenOnPort = EXIT_TCP_LISTENING_BASE + i;
        PacketCollectors.push(new PacketCollector(listenOnPort));
    }
    client = new UDPCollector(EXIT_UDP_OUT, PacketCollectors);

    var tunnel = childProcess.spawn('./udptunnel', [
        // '-vvv', // verbose
        '-s',
        EXIT_UDP_OUT,
    ]);

    function onClose(){
        tunnel.kill('SIGINT');
        tunnel.on('close', function(){
            console.log('Remote tunnel exit closed.');
        });
        client.close();
        for(var i=0; i<PacketCollectors.length; i++){
            PacketCollectors[i].close();
        }
    }
    process.once('exit', onClose);
    process.once('SIGINT', onClose);

    var str = "TCP Packets now sending to ports ";
    for(var i=0; i<TUNNELS; i++) str += (EXIT_TCP_LISTENING_BASE + i) + ",";
    str += " will be delivered as UDP to Port [" + EXIT_UDP_OUT  + "]";
    console.log(str);
}

buildExit();
buildEntrance();
