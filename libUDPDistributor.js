/*
 * Listens on the UDPTunnel output, and distribute UDP Packets to
 * PacketForwarder.
 */
var dgram = require('dgram'),
    events = require('events'),
    util = require('util'),
    net = require('net');

function UDPDistributor(serverPort, PacketForwarders){
    var self = this;
    events.EventEmitter.call(this);

    var server = dgram.createSocket('udp4');
    server.bind(serverPort);

    var peer = null; // for sending back messages
    server.on('message', function(data, sender){
        peer = sender; // bind to this sender
        // choose a PacketForwarder randomly
        var i = Math.floor(Math.random(PacketForwarders.length));
        PacketForwarders[i].send(data);
        console.log("Local : UDPTunnel.Client -> Packet", data);
    });

    for(var i=0; i<PacketForwarders.length; i++){
        PacketForwarders[i].on('data', function(data){
            // send data back to peer.
            server.send(data, 0, data.length, peer.port, peer.address);
            console.log("Local : UDPTunnel.Client <- Packet", data);
        });
    }

    this.close = function(){
        try{
            server.close();
        } catch(e){
        }
    }

    return this;
}
util.inherits(UDPDistributor, events.EventEmitter);
module.exports = UDPDistributor;
