var events = require('events'),
    buffer = require('buffer'),
    util = require('util');

function PacketStream(){
    var self = this;
    events.EventEmitter.call(this);

    var buffers = []

    this.pack = function(buffer){
        return '\n' + buffer.toString('base64') + '\n';
    }

    this.unpack = function(buf){
        /* given `buffers` a list of buffer, check if they can emit a packet
         * when joined together. */
        buffers.push(buf);
        var jbuf = buffer.Buffer.concat(buffers),
            start = false, pointer = 0;
        if(jbuf.length < 2) return;
        for(pointer=0; pointer < jbuf.length; pointer++){
            if(0x0A == jbuf[pointer]){
                if(false === start){
                    start = pointer;
                    continue;
                }
                var packet = new buffer.Buffer(pointer - start);
                jbuf.copy(packet, 0, start, pointer);
                start = pointer;
                try{
                    var decodePacket = packet.toString();
                    var newPacket = new buffer.Buffer(decodePacket, 'base64');
                    if(newPacket.length > 1){
                        self.emit('data', newPacket);
                    }
                } catch(e){
                }
            }
        }
        if(false === start) start = 0;
        var remainBuffer = jbuf.slice(start);
        if(remainBuffer.length > 0){
            buffers = [remainBuffer];
        } else {
            buffers = [];
        }
    }

    return this;
}
util.inherits(PacketStream, events.EventEmitter);
module.exports = PacketStream;

/*
var ps1 = new PacketStream(), ps2 = new PacketStream();
ps2.on('data', console.log)

var pk1 = new buffer.Buffer(ps1.pack(new buffer.Buffer('0123456')));
ps2.unpack(pk1)
ps2.unpack(new buffer.Buffer('\nbbbaaabbbaaabbb\n'))
*/
