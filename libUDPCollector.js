var dgram = require('dgram'),
    events = require('events'),
    util = require('util'),
    net = require('net');

function UDPCollector(destPort, PacketCollectors){
    var self = this;
    events.EventEmitter.call(this);

    var client = dgram.createSocket('udp4'); // for sending data
    client.bind();

    this.close = function(){
        try{
            client.close();
        } catch(e){
        }
    }

    client.on('message', function(data){
        // choose a PacketForwarder randomly
        var i = Math.floor(Math.random(PacketCollectors.length));
        PacketCollectors[i].send(data);
        console.log("Remote: Packet <- UDPTunnel.Server", data);
    });

    for(var i=0; i<PacketCollectors.length; i++){
        PacketCollectors[i].on('data', function(data){
            console.log("Remote: Packet -> UDPTunnel.Server", data);
            client.send(data, 0, data.length, destPort);
        });
    }

    return this;
}
util.inherits(UDPCollector, events.EventEmitter);
module.exports = UDPCollector;
