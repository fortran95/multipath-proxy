/*
 * This module creates a connection that receives local packets, and writes
 * them to the TCP listening ports. After connection to TCP ports established, 
 * packets emitted from TCP will be also sent backwards.
 */
var net = require('net'),
    events = require('events'),
    util = require('util');
var packetStream = require('./libPacketStream.js');
//////////////////////////////////////////////////////////////////////////////

function Packet2TCP(destPort){
    var self = this;
    events.EventEmitter.call(this);

    var socket = new net.Socket();
    var connected = false;
    socket.connect(destPort);

    socket.on('error', function(err){
        self.emit('error', err);
    });

    var pstream = new packetStream();
    pstream.on('data', function(data){ self.emit('data', data); });

    var buffers = [];
    this.send = function(data){
        if(data) buffers.push(data);
        if(!connected) return;
        while(buffers.length > 0){
            socket.write(pstream.pack(buffers.shift()));
        }
    }

    socket.on('connect', function(){
        connected = true;
        self.send();
    });

    socket.on('data', pstream.unpack);

    this.close = function(){
        try{
            socket.destroy();
        } catch(e){
        }
        self.send = function(){};
    }
    socket.on('close', self.close);

    return this;
}
util.inherits(Packet2TCP, events.EventEmitter);

//////////////////////////////////////////////////////////////////////////////

module.exports = Packet2TCP;

/*
var test = new Packet2TCP(4000);
test.send('HTTP/1.1\n\rGET /\n\r\n\r');
test.on('data', function(d){
    console.log(d.toString())
});
*/
