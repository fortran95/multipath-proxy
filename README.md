Multipath-Proxy
===============

This is a proxy system making use of several components.

The most important component of this system is the `udptunnel` tool. It
establishes a single TCP connection over a UDP based tunnel. The NodeJS part
of this program, which can be run using `node index.js`, is practically a UDP
over TCP tunnel system making use of several TCP connections.

The data flow is:

1) The local browser or similar starts a request.
2) The request is capsuled by the `udptunnel` program.
3) The encapsuled data is now UDP. It will be received by our program. Our
   program chooses a TCP tunnel randomly and sends the UDP Packet with that.
4) On the server, the UDP Packet goes over the TCP tunnel exit, and will be
   sent to the server part of the `udptunnel` program using Node.js
5) And so the data connection is established. Reversed data flow is also
   possible.

The goal of this project is making a TCP tunnel to a remote server using use of
multiple different connections:

The secondary TCP tunnels between the server and the client's computer can be
established using a lot of protocols: SSH, HTTP or even XMPP. With some
modifications on the program it may also no more needs to be TCP, UDP relays
based on VoIP is also considerable. This should make it harder to analysis and
control the traffic at the firewall, and make the connection more robust.
