
/*
 * This module creates a listening port(normally on the server side). The port
 * listens for incoming TCP connections, and emits the received packets as
 * events. The program may also reply packets, which will be returned to
 * the TCP connection.
 *
 * The TCP server collects all TCP packets and emits them immediately as if
 * they are UDP packets from the same sender.
 */
var net = require('net'),
    events = require('events'),
    buffer = require('buffer'),
    util = require('util');
var packetStream = require('./libPacketStream.js');
//////////////////////////////////////////////////////////////////////////////

function TCP2Packet(localPort){
    var self = this;
    events.EventEmitter.call(this);

    var socket = new net.createServer();

    var pstream = new packetStream();
    pstream.on('data', function(data){ self.emit('data', data); });

    var sendFuncs = [];
    this.send = function(data){
        /* if we have got a connection, send this packet. Since packets
         * are designed to be UDP carried packets, in case of unavailability of
         * connections just drop the packet. */
        for(var i in sendFuncs){
            if(null == sendFuncs[i]) continue;
            if(!sendFuncs[i](pstream.pack(data))){
                sendFuncs[i] = null;
            }
        }
        // XXX TODO remove stale senders by testing them with calls without
        // parameter. If returned False as defined in following function
        // factory, should remove it.
    }

    socket.on('connection', function(socket){
        /* Buffer the incoming stream within one TCP connection, and
         * extract the encoded packets.*/
        var buffers = [];
        socket.on('data', pstream.unpack);
        sendFuncs.push((function sendFuncFactory(sck){
            var canSend = true;
            sck.on('error', function(){
                canSend = false;
                sck.destroy();
            });
            sck.on('close', function(){ canSend = false; });
            return function sendFunc(data){
                if(data) sck.write(data);
                return canSend;
            }
        })(socket));
    });

    socket.listen(localPort);

    this.close = function(){
        try{
            socket.close();
        } catch(e){
        }
        self.send = function(){};
    }
    socket.on('close', self.close);

    return this;
}
util.inherits(TCP2Packet, events.EventEmitter);

//////////////////////////////////////////////////////////////////////////////

module.exports = TCP2Packet;
